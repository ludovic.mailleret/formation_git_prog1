# import numpy as np

from utils.func_TIS import *

etat0 = np.array([10., 15.])
mS = 3.

print('Ce programme simule un modèle TIS.')
print('Voulez-vous :')
print('1 - simuler les dynamiques contre le temps')
print('2 - simuler les trajectoires dans le plan de phase')

foo = input('votre choix : ')

if foo == '1':
    Fig = plotSimAll(mS, params_sim, tspan = tspan)
    plt.show()
elif foo == '2':
    Fig = plotPlane(mS, params_sim)
    plt.show()
else:
    print('Erreur')
